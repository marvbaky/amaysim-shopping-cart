const ultSmall = Product.new({
    productCode: 'ult_small',
    productName: 'Unlimited 1GB',
    unitPrice: 24.90
});

const ultMedium = Product.new({
    productCode: 'ult_medium',
    productName: 'Unlimited 2GB',
    unitPrice: 29.90
});

const ultLarge = Product.new({
    productCode: 'ult_large',
    productName: 'Unlimited 5GB',
    unitPrice: 44.90
});

const oneGbDataPack = Product.new({
    productCode: '1gb',
    productName: '1 GB Data-pack',
    unitPrice: 9.90
});

const customOffers = [];
const promoCodeOffers = [];

customOffers.push(XForYOffer.new({
    product: ultSmall,
    requiredCount: 3,
    bundleToBePaidCount: 2,
}));

customOffers.push(BulkDiscountOffer.new({
    product: ultLarge,
    discountUnitPrice: 39.90,
    requiredCount: 3,
}));

customOffers.push(BundleOffer.new({
    product: ultMedium,
    productToBundle: oneGbDataPack,
    requiredCount: 1,
    numberOfItemsToBundle: 1,
}));

promoCodeOffers.push(CodeOffer.new({
    promoCode: PromoCode.new({
        code: 'I<3AMAYSIM',
        discountPercent: 10
    }),
}));

const pricingRules = PricingRules.new({
    customOffers: customOffers,
    promoCodeOffers: promoCodeOffers,
});

