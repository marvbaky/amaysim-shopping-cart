# Amaysim Shopping Cart Developer Guide
## Running the application
* Open index.html. It has all the javascript imported in it.

## Creating new Products
* To create a new product, you can call the Product API object. It will return a new object instance of `Product`
```
const ultSmall = Product.new({
    productCode: 'ult_small',
    productName: 'Unlimited 1GB',
    unitPrice: 24.90
});
```

## Creating Offers
* To create a new offer, you'll need to choose between the CustomOffer and PromoCodeOffer API objects. This is because these 2 entities have different business rules.
### Creating a Custom Offer
We have currently 3 offers, which can be customized based on the need.
* XForY Offer
  * This is a template for x for y deal. To use this, you'll need to supply the following parameters:
    * product - the product object it is offered for.
    * requiredCount - the number of required items before this offer is applied.
    * bundledToBePaidCount - the reduced number of items that the customer will now only play.
```
customOffers.push(XForYOffer.new({
    product: ultSmall,
    requiredCount: 3,
    bundleToBePaidCount: 2,
}));
```
* BulkDiscount Offer
  * This is a template for discounts that is applied when a certain number of quantities was reached. To use this, supply the following parameters:
    * product - the product object it is offered for.
    * discountUnitPrice - the new unit priced use when this bundle is applied.
    * requiredCount - the number of required items before this offer is applied.
```
customOffers.push(BulkDiscountOffer.new({
    product: ultLarge,
    discountUnitPrice: 39.90,
    requiredCount: 3,
}));
```
* Bundle Offer
  * This is a template for an offer where a certain product can be bundled alongside the original product a user has added. To use this, supply the following parameters:
    * product - the product object it is offered for.
    * productToBundle - the product object that will be bundled.
    * requiredCount - the number of required items of the original product before this offer is applied.
    * numberOfItemsToBundle - the number of the bundled product that will be added.
```
customOffers.push(BundleOffer.new({
    product: ultMedium,
    productToBundle: oneGbDataPack,
    requiredCount: 1,
    numberOfItemsToBundle: 1,
}));
```

### Creating a Promo Code Offer
We currently have 1 template for creating promo code offers. This is a template for applying discount codes based on a Promo Code
* Code Offer
  * This is a template to apply discount codes across the whole cart. To use this, supply the following parameters:
  * promoCode - the promoCode object that the offer is associated with.
```
promoCodeOffers.push(CodeOffer.new({
    promoCode: PromoCode.new({
        code: 'I<3AMAYSIM',
        discountPercent: 10
    }),
}));
```

## Creating the Pricing Rules Service
The Pricing Rule Service is a service that will apply different offers created. To create this, you'll need to provide a list of Custom Offers and Promo Code Offer. Custom Offers are applied first before the Promo Code Offer.
```
const pricingRules = PricingRules.new({
    customOffers: customOffers,
    promoCodeOffers: promoCodeOffers,
});
```

## Creating a Shopping Cart
To create a shopping cart, it should be supplied with a pricing rule.
```
const cart = ShoppingCart.new(pricingRules);
```

## Adding to a Shopping Cart
To add an item to a shopping cart, a `CartItem` object must be created. Follow the steps below on creating a `CartItem` and adding it to your cart
```
const item1 = CartItem.new({ product: ultSmall, quantity: 4});
cart.add(item1);
```
You may also include a promocode when adding a item to your cart. Promo code are only added once. 
```
cart.add(item1, 'promo-code');
```

## Checking out your Shopping Cart
To see the total price and all applied offers, you must checkout your cart. This means that any Bundle Offered product will not yet be added to your cart until you declare it for checkout. To Checkout, use the following command:
```
cart.total;
```

## Checking your items
To check all added items, you may use the following command:
```
cart.items
```
Note that any bundled offered products are not yet added if checkout is not yet initiated. Also, if new items are added, some details might reset as offers would need to be reapplied on checkout.

## Testing the application
For each scenario provided, here are the commands that you'll just need to copy and paste to test every outputs:

### Test Case 1
```
const cart = ShoppingCart.new(pricingRules);
cart.add(CartItem.new({product:ultSmall,quantity:3}));
cart.add(CartItem.new({product:ultLarge,quantity:1}));
cart.total;
cart.items;
```

### Test Case 2
```
const cart = ShoppingCart.new(pricingRules);
cart.add(CartItem.new({product:ultSmall,quantity:2}));
cart.add(CartItem.new({product:ultLarge,quantity:4}));
cart.total;
cart.items;
```

### Test Case 3
```
const cart = ShoppingCart.new(pricingRules);
cart.add(CartItem.new({product:ultSmall,quantity:1}));
cart.add(CartItem.new({product:ultMedium,quantity:2}));
cart.total;
cart.items;
```

### Test Case 4
```
const cart = ShoppingCart.new(pricingRules);
cart.add(CartItem.new({product:ultSmall,quantity:1}));
cart.add(CartItem.new({product:oneGbDataPack,quantity:1}), 'I<3AMAYSIM');
cart.total;
cart.items;
```