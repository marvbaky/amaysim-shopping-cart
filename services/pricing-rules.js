const PricingRules = {
    new: (options) => {
        return {
            customOffers: options?.customOffers,
            promoCodeOffers: options?.promoCodeOffers,
            applyRules: function(items, promos) {
                this.customOffers.forEach(co => co.applyOffer(items));
                this.promoCodeOffers.forEach(pco => pco.applyOffer(promos));
            }
        }
    }
};