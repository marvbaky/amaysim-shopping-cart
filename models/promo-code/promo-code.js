const PromoCode = {
    new: (options) => {
        return {
            code: options?.code,
            discountPercent: options?.discountPercent
        }
    }
}