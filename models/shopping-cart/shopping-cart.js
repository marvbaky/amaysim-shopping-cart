const ShoppingCart = {
    new: (pricingRules) => {
        return {
            userItems: [],
            promos: [],
            add: function (item, promoCode) {                
                if (item) {
                    const duppItem = this.userItems.find(i => i.product.productCode === item.product.productCode);
                    if (duppItem) {
                        duppItem.quantity += item.quantity;
                        duppItem.quantityToBePaid = duppItem.quantity;
                    } else {
                        this.userItems.push(item);
                    }                    
                }

                //No specific rule regarding this but assumed that all codes should be unique
                //Must asked for specific business rules
                if (promoCode) {
                    if (!this.promos.some(p => p.code === promoCode)) {
                        this.promos.push(CartPromo.new({
                            code: promoCode
                        }));
                    }                    
                }
            },
            get total() {
                pricingRules.applyRules(this.userItems, this.promos);
                let totalPrice = this.userItems.reduce(
                    (prevItem, currentItem) => prevItem + currentItem.officialTotalPrice, 
                    0
                );
                
                //No specific rules with regards how discount is applied
                //Currently, this will apply the discount on each succeeding totalPrice computed.
                //It will not discount from the original price nor will it be an
                //accumulated discount percent
                this.promos.forEach(promo => {
                    if (promo.isValid) {
                        totalPrice -= totalPrice * (promo.discountPercent / 100);
                    }                    
                });

                return totalPrice.toFixed(2);
            },
            get items() {
                const allItems = this.userItems.map(i => {
                    return {
                        productName: i.product.productName,
                        quantity: i.quantity,
                        officialUnitPrice: i.officialUnitPrice,
                        quantityToBePaid: i.quantityToBePaid,
                        officialTotalPrice: i.officialTotalPrice
                    }
                })

                this.userItems.forEach(i => {
                    if (i.bundledItems.length) {
                        const bundledItems = i.bundledItems.map(i => {
                            return {
                                productName: i.product.productName,
                                quantity: i.quantity,
                                officialUnitPrice: i.officialUnitPrice,
                                quantityToBePaid: i.quantityToBePaid,
                                officialTotalPrice: i.officialTotalPrice
                            }
                        })
                        allItems.push(...bundledItems);
                    }
                })

                return allItems;
            }
        }
    }
};