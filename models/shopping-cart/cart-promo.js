const CartPromo = {
    new: (options) => {
        return {
            code: options?.code,
            isValid: false,
            discountPercent: 0
        }
    }
}