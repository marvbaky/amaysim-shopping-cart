const CartItem = {
    new: (options) => {
        return {
            product: options?.product,
            quantity: options?.quantity,
            officialUnitPrice: options?.product.unitPrice,
            quantityToBePaid: options?.quantity,
            bundledItems: [],
            get officialTotalPrice() {
                return this.officialUnitPrice && this.quantityToBePaid ? this.officialUnitPrice * this.quantityToBePaid : 0
            },
        }
    }
}