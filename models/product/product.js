const Product = {
    new: (options) => {
        return {
            productCode: options?.productCode,
            productName: options?.productName,
            unitPrice: options?.unitPrice
        }
    } 
}