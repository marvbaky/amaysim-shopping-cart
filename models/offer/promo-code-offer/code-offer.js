const CodeOffer =  {
    new: (options) => {
        return {
            promoCode: options?.promoCode,
            applyOffer: function(codes) {
                const code = codes.find(c => c.code === this.promoCode.code);

                if (code) {
                    code.isValid = true;
                    code.discountPercent = this.promoCode.discountPercent;
                }
            }
        }
    }
}