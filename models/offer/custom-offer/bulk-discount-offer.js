const BulkDiscountOffer = {
    new: (options) => {
        return {
            product: options?.product,
            discountUnitPrice: options?.discountUnitPrice,
            requiredCount: options?.requiredCount,
            applyOffer: function(items) {
                const item = items.find(i => i.product.productCode === this.product.productCode);

                if (item) {
                    if (item.quantity > this.requiredCount) {
                        item.officialUnitPrice = this.discountUnitPrice;
                    }
                };
            }
        }
    }
}