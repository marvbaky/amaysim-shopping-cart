const BundleOffer = {
    new: (options) => {
        return {
            product: options?.product,
            productToBundle: {...options?.productToBundle},
            requiredCount: options?.requiredCount,
            numberOfItemsToBundle: options?.numberOfItemsToBundle,
            applyOffer: function(items) {
                const item = items.find(i => i.product.productCode === this.product.productCode);

                if (item) {
                    const countOffersToApplyForItem = parseInt(item.quantity / this.requiredCount, 10);
                    if (countOffersToApplyForItem >= 1) {
                        const bundledItem = item.bundledItems.find(i => i.product.productCode === this.productToBundle.productCode);

                        if (bundledItem) {
                            bundledItem.quantity = countOffersToApplyForItem * this.numberOfItemsToBundle
                            bundledItem.quantityToBePaid = bundledItem.quantity;
                        } else {
                            this.productToBundle.unitPrice = 0;
                                item.bundledItems.push(CartItem.new({
                                    product: this.productToBundle, 
                                    quantity: countOffersToApplyForItem * this.numberOfItemsToBundle}));     
                        }
                    }                                                
                }                
            }
        }
    }
}