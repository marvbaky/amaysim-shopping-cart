const XForYOffer = {
    new: (options) => {
        return {
            product: options?.product,
            requiredCount: options?.requiredCount,
            bundleToBePaidCount: options?.bundleToBePaidCount,
            applyOffer: function(items) {
                const item = items.find(i => i.product.productCode === this.product.productCode);

                if (item) {
                    const countOffersToApplyForItem = parseInt(item.quantity / this.requiredCount, 10);
                    if (countOffersToApplyForItem >= 1) {
                        const countOfItemsToBeRemoved = countOffersToApplyForItem * this.requiredCount;
                        const countOfItemsToBePaid = countOffersToApplyForItem * this.bundleToBePaidCount;

                        item.quantityToBePaid = item.quantity - countOfItemsToBeRemoved;
                        item.quantityToBePaid += countOfItemsToBePaid;
                    }
                }
            }
        }
    }
}